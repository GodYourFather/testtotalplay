package com.example.testtotalplay.domain

import com.example.testtotalplay.data.LoginRequestModel
import com.example.testtotalplay.data.SessionModel
import com.example.testtotalplay.di.PostRepository
import com.example.testtotalplay.util.SessionManager
import com.example.testtotalplay.util.URLConstants.Companion.urlLogin
import com.example.testtotalplay.util.Utilities
import javax.inject.Inject


class LoginUseCase @Inject constructor(
    private val repository: PostRepository,
) {
    suspend operator fun invoke(user: String, password: String): Pair<SessionModel?, SessionModel?> {

        val response = repository.getPostRepository(urlLogin, LoginRequestModel(user = user, password = password))

        if(response.isSuccessful){
            response.body()?.let {

                SessionManager.getInstance().setSession(it)

                return Pair(it, null)
            }?: run {
                val session = SessionModel("Error")
                return Pair(null, session)
            }
        }else{
            response.errorBody()?.let {

                val entity = Utilities.genericParser(
                    it.byteStream().readBytes().decodeToString(),
                    SessionModel::class.java
                ) as SessionModel

                return Pair(null, entity)
            }?: run{
                val session = SessionModel("Error")
                return Pair(null, session)
            }
        }
    }
}