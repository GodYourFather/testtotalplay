package com.example.testtotalplay.domain

import com.example.testtotalplay.data.ReferencesResponseModel
import com.example.testtotalplay.data.SessionModel
import com.example.testtotalplay.di.PostRepository
import com.example.testtotalplay.util.SessionManager
import com.example.testtotalplay.util.URLConstants.Companion.urlReferences
import com.example.testtotalplay.util.Utilities
import javax.inject.Inject

class GetListReferencesUseCase @Inject constructor(
    private val repository: PostRepository,
) {
    suspend operator fun invoke(): Pair<ReferencesResponseModel?, SessionModel?> {
        val response = repository.getReferencesPostRepository(
            urlReferences,
            SessionModel(session = SessionManager.getInstance().getSession().session)
        )

        if(response.isSuccessful){
            response.body()?.let {
                return Pair(it, null)
            }?: run {
                val session = SessionModel("Error")
                return Pair(null, session)
            }
        }else{
            response.errorBody()?.let {

                val entity = Utilities.genericParser(
                    it.byteStream().readBytes().decodeToString(),
                    SessionModel::class.java
                ) as SessionModel

                return Pair(null, entity)
            }?: run{
                val session = SessionModel("Error")
                return Pair(null, session)
            }
        }
    }
}