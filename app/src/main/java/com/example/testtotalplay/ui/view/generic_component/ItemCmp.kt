package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.AsyncImage
import com.example.testtotalplay.R
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil

@Composable
fun ItemCmp(
    ref: String,
    image: String,
){
    Column(modifier = Modifier
        .background(MaterialTheme.colors.background)
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth(),
            elevation = ELEVATION_4,
            backgroundColor = MaterialTheme.colors.background,
        ){
            Row(modifier = Modifier
                .padding(PADDING_16),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {

                if(image != StringUtil.EMPTY_STR){
                    AsyncImage(
                        model = image,
                        contentDescription = StringUtil.EMPTY_STR
                    )
                }else{
                    Image(
                        painter = painterResource(id = R.drawable.ic_baseline_hide_image_24),
                        contentDescription = StringUtil.EMPTY_STR
                    )
                }

                CustomHorizontalDividerCmp(width = DIVIDER_8)

                Column {
                    val text1 = listOf(
                        AnnotatedString(
                            ref,
                            textGrey700SpanStyle
                        ),
                    )

                    AutoResizeTextCmp(
                        modifier = Modifier
                            .fillMaxWidth(),
                        fontSize = FONT_SIZE_14,
                        textAlign = TextAlign.Start,
                        overflow = TextOverflow.Ellipsis,
                        annotatedStringList = text1,
                        maxLines = 3
                    )

                }
            }
        }
        CustomVerticalDividerCmp(height = DIVIDER_8)
    }
}

@Composable
@Preview(showBackground = true)
fun ItemCmpPreview(){
    ItemCmp(
        ref = StringUtil.EMPTY_STR,
        image = StringUtil.EMPTY_STR
    )
}