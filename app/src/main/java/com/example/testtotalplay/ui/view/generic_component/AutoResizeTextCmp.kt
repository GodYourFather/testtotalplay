package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.example.testtotalplay.ui.theme.FONT_SIZE_14
import com.example.testtotalplay.ui.theme.MuseoSans300
import com.example.testtotalplay.ui.theme.black300SpanStyle
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil
import com.example.testtotalplay.util.StringUtil.Companion.test_text

@Composable
fun AutoResizeTextCmp(
    modifier: Modifier = Modifier,
    fontSize: TextUnit,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration = TextDecoration.None,
    textAlign: TextAlign? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    annotatedStringList: List<AnnotatedString> = listOf(AnnotatedString(StringUtil.EMPTY_STR, SpanStyle(
        fontFamily = MuseoSans300,
        color = MaterialTheme.colors.primary
    )))
) {
    Text(
        buildAnnotatedString {
            for (annotatedStr in annotatedStringList) {
                withStyle(
                    style = annotatedStr.spanStyle
                ) {
                    append(annotatedStr.text)
                }
            }
        },
        maxLines = maxLines,
        fontStyle = fontStyle,
        fontWeight = fontWeight,
        fontFamily = fontFamily,
        letterSpacing = letterSpacing,
        textDecoration = textDecoration,
        textAlign = textAlign,
        lineHeight = lineHeight,
        overflow = overflow,
        softWrap = softWrap,
        fontSize = fontSize.value.sp,
        onTextLayout = {},
        modifier = modifier
    )
}

@Composable
@Preview(showBackground = true)
fun AutoResizeTextCmpPreview(){
    AutoResizeTextCmp(
        annotatedStringList = listOf(AnnotatedString(test_text, SpanStyle(
            fontFamily = MuseoSans300,
            color = MaterialTheme.colors.primary
        )
        )),
        fontSize = FONT_SIZE_14
    )
}

