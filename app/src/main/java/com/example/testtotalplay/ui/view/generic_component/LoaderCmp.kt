package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import com.airbnb.lottie.compose.*
import com.example.testtotalplay.R
import com.example.testtotalplay.ui.theme.DefaultBackGroundDialogIntercam

@Composable
fun LoaderCmp(
    boolSeeLoader: MutableState<Boolean>
) {

    when(boolSeeLoader.value){
        true -> {
            val compositionResult: LottieCompositionResult =
                rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.loader_animation))

            val progress by animateLottieCompositionAsState(
                composition = compositionResult.value,
                isPlaying = true,
                iterations = LottieConstants.IterateForever,
                speed = 5.0f
            )
            ConstraintLayout(modifier = Modifier
                .fillMaxSize()
                .clickable(enabled = false, onClick = {})
                .background(DefaultBackGroundDialogIntercam)
            ) {
                val (content) = createRefs()
                Column(modifier = Modifier
                    .fillMaxWidth(.6f)
                    .constrainAs(content){
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                    }
                ) {
                    LottieAnimation(
                        composition = compositionResult.value,
                        progress = progress)
                }
            }
        }
        false -> {

        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoaderCmpPreview(){
    LoaderCmp(boolSeeLoader = remember { mutableStateOf(true) })
}