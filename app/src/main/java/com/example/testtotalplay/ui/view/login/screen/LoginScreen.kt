package com.example.testtotalplay.ui.view.login.screen

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.testtotalplay.navigation.Destinations
import com.example.testtotalplay.ui.view.generic_component.ErrorAlertCmp
import com.example.testtotalplay.ui.view.generic_component.LoaderCmp
import com.example.testtotalplay.ui.view.login.view.LoginView
import com.example.testtotalplay.ui.view.login.viewmodel.LoginViewModel
import com.example.testtotalplay.util.ResourceState
import com.example.testtotalplay.util.StringUtil

@Composable
fun LoginScreen(
    navController: NavHostController
) {

    val viewModel: LoginViewModel = hiltViewModel()
    val serviceLogin = viewModel.getValueServiceLogin().collectAsState()

    when(serviceLogin.value){
        ResourceState.Success -> {
            navController.navigate(route = Destinations.Welcome.route) {
                launchSingleTop = true
            }
            viewModel.setValueUser(StringUtil.EMPTY_STR)
            viewModel.setValuePassword(StringUtil.EMPTY_STR)
            viewModel.baseViewModel.setErrorAlertMessage(StringUtil.EMPTY_STR)
            viewModel.baseViewModel.hideErrorAlert()
            viewModel.setValueServiceLogin(ResourceState.Default)
        }
        ResourceState.Failure -> {
            viewModel.setValueServiceLogin(ResourceState.Default)
        }
        ResourceState.Loading -> {
            viewModel.setValueServiceLogin(ResourceState.Default)
        }
        ResourceState.Default -> {
        }
    }

    LoginView(
        onClickContinue = {
            viewModel.login()
        },
        user = viewModel.getValueUser(),
        password = viewModel.getValuePassword()
    )

    LoaderCmp(viewModel.baseViewModel.getBoolSeeLoader())

    ErrorAlertCmp(
        boolSeeErrorAlert = viewModel.baseViewModel.getBoolSeeErrorAlert(),
        strMessage = viewModel.baseViewModel.getErrorAlertMessage(),
        closeOnClick = {}
    )

    BackHandler {
    }
}