package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.example.testtotalplay.ui.theme.DIVIDER_8
import com.example.testtotalplay.ui.theme.HEIGHT_10
import com.example.testtotalplay.ui.theme.WIDTH_0

@Composable
fun CustomHorizontalDividerCmp(
    modifier: Modifier = Modifier,
    width: Dp = WIDTH_0) {
    Divider(
        color = Color.Transparent,
        modifier = modifier
            .height(HEIGHT_10)
            .width(width)
    )
}
@Composable
@Preview(showBackground = true)
fun CustomHorizontalDividerCmpPreview(){
    CustomHorizontalDividerCmp(width = DIVIDER_8)
}