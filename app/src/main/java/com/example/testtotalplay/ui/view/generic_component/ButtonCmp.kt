package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil


@Composable
fun ButtonCmp(
    modifier: Modifier = Modifier,
    text: String,
    buttonImage: Int? = null,
    onClick: () -> Unit,
    enabled: Boolean = true,
    maxLines: Int = 3,
    textSize: TextUnit = FONT_SIZE_16,
    spanStyle: SpanStyle = SpanStyle(
        fontFamily = MuseoSans900,
        color = MaterialTheme.colors.secondary),
    backgroundColor: Color = ButtonBlue,
) {
    val annotatedStrBtnList =
        listOf(
            AnnotatedString(text = text, spanStyle = spanStyle)
        )
    Button(
        modifier = modifier
            .fillMaxWidth()
            .height(BUTTON_HEIGHT),
        onClick = onClick,
        enabled = enabled,
        colors = ButtonDefaults.buttonColors(backgroundColor = backgroundColor),
        shape = RoundedCornerShape(50),
    ) {
        if (buttonImage != null) {
            Image(
                painter = painterResource(id = buttonImage),
                contentDescription = StringUtil.EMPTY_STR,
            )
            CustomHorizontalDividerCmp(width = PADDING_16)
        }
        AutoResizeTextCmp(
            maxLines = maxLines,
            fontSize = textSize,
            textAlign = TextAlign.Center,
            overflow = TextOverflow.Ellipsis,
            annotatedStringList = annotatedStrBtnList
        )
    }
}
@Preview
@Composable
fun ButtonCmpPreview(){
    ButtonCmp(
        text = StringUtil.test_text,
        buttonImage = null,
        backgroundColor = ButtonBlue,
        onClick = {
        }
    )
}
