package com.example.testtotalplay.ui.view.welcome.screen

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.testtotalplay.ui.view.generic_component.ErrorAlertCmp
import com.example.testtotalplay.ui.view.generic_component.LoaderCmp
import com.example.testtotalplay.ui.view.welcome.view.WelcomeView
import com.example.testtotalplay.ui.view.welcome.viewmodel.WelcomeViewModel
import com.example.testtotalplay.util.SessionManager

@Composable
fun WelcomeScreen(
    navController: NavHostController
) {
    val viewModel: WelcomeViewModel = hiltViewModel()

    WelcomeView(
        onClickBack = {
            SessionManager.getInstance().clean()
            navController.popBackStack()
        },
        references = viewModel.getValueListReferences()
    )

    LoaderCmp(viewModel.baseViewModel.getBoolSeeLoader())

    ErrorAlertCmp(
        boolSeeErrorAlert = viewModel.baseViewModel.getBoolSeeErrorAlert(),
        strMessage = viewModel.baseViewModel.getErrorAlertMessage(),
        closeOnClick = {}
    )

    BackHandler {
    }
}