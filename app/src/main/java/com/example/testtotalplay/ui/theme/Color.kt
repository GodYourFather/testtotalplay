package com.example.testtotalplay.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Text = Color(0xFF000000)
val TextGrey = Color(0xFF3D3C3C)
val InputGrey = Color(0xFFf6f6f6)

val Blue = Color(0xFF005da6)
val ButtonBlue = Color(0xFF3590D8)

val DefaultBackGround = Color(0xFFfdfdfd)
val DefaultBackGroundBlack = Color(0xFF1F1E1E)

val DisableDividerCode = Color(0xFFd7d7d7)

val DefaultBackGroundDialogIntercam = Color(0x99000000)