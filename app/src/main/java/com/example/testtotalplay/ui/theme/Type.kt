package com.example.testtotalplay.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.testtotalplay.R

// Set of Material typography styles to start with

val MuseoSans100 = FontFamily(Font(R.font.museo_sans_100))
val MuseoSans300 = FontFamily(Font(R.font.museo_sans_300))
val MuseoSans500 = FontFamily(Font(R.font.museo_sans_500))
val MuseoSans700 = FontFamily(Font(R.font.museo_sans_700))
val MuseoSans900 = FontFamily(Font(R.font.museo_sans_900))

val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
    /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)

// Black -------------------------------------

val black300SpanStyle = SpanStyle(
    fontFamily = MuseoSans300,
    color = Color.Black
)
val black700SpanStyle = SpanStyle(
    fontFamily = MuseoSans700,
    color = Color.Black
)


// Blue -------------------------------------
val blue500SpanStyle = SpanStyle(
    fontFamily = MuseoSans500,
    color = Blue
)

//TextGrey --------------------------------

val textGrey700SpanStyle = SpanStyle(
    fontFamily = MuseoSans700,
    color = TextGrey
)

// White -------------------------------------
val white900SpanStyle = SpanStyle(
    fontFamily = MuseoSans900,
    color = Color.White
)
val white700SpanStyle = SpanStyle(
    fontFamily = MuseoSans700,
    color = Color.White
)

