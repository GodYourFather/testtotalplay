package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.example.testtotalplay.R
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil

@Composable
fun HeaderDialogCmp(
    annotatedStrTitle: List<AnnotatedString>,
    seeClose: Boolean,
    seeDivider: Boolean,
    onClickClose: () -> Unit = {},
){
    Column(modifier = Modifier
        .fillMaxWidth()) {
        ConstraintLayout(modifier = Modifier
            .padding(PADDING_16)
            .fillMaxWidth()
        ) {

            val (Text, Close) = createRefs()

            AutoResizeTextCmp(
                maxLines = 2,
                modifier = Modifier
                    .fillMaxWidth(WEIGHT_90)
                    .constrainAs(Text) {
                        start.linkTo(parent.start)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                    },
                fontSize = FONT_SIZE_18,
                textAlign = TextAlign.Start,
                overflow = TextOverflow.Ellipsis,
                annotatedStringList = annotatedStrTitle
            )

            if(seeClose){
                Image(modifier = Modifier
                    .width(18.dp)
                    .height(18.dp)
                    .clickable {
                        onClickClose()
                    }
                    .constrainAs(Close) {
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                    },
                    painter = painterResource(id = R.drawable.ic_baseline_close_24),
                    contentDescription = StringUtil.EMPTY_STR
                )
            }
        }

        if(seeDivider){
            CustomVerticalDividerCmp(
                height = DIVIDER_1,
                color = DisableDividerCode
            )
        }
    }
}

@Composable
@Preview(showBackground = true)
fun HeaderDialogCmpPreview(){
    HeaderDialogCmp(
        annotatedStrTitle = listOf(AnnotatedString(StringUtil.test_text, SpanStyle(
            fontFamily = MuseoSans300,
            color = MaterialTheme.colors.primary
        )
        )),
        seeClose = true,
        seeDivider = true,
        onClickClose = {},
    )
}