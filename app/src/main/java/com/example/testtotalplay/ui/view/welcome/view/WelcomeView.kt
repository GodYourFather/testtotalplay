package com.example.testtotalplay.ui.view.welcome.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.testtotalplay.data.ReferencesResponseModel
import com.example.testtotalplay.ui.theme.DIVIDER_16
import com.example.testtotalplay.ui.theme.FONT_SIZE_16
import com.example.testtotalplay.ui.theme.PADDING_16
import com.example.testtotalplay.ui.theme.textGrey700SpanStyle
import com.example.testtotalplay.ui.view.generic_component.AutoResizeTextCmp
import com.example.testtotalplay.ui.view.generic_component.CustomVerticalDividerCmp
import com.example.testtotalplay.ui.view.generic_component.HeaderCmp
import com.example.testtotalplay.ui.view.generic_component.ItemCmp
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil
import com.example.testtotalplay.util.StringUtil.Companion.use_the_bank_reference_to_pay_at_participanting_banks_text

@Composable
fun WelcomeView(
    onClickBack:() -> Unit,
    references: MutableState<ReferencesResponseModel>
) {
    Scaffold(
        topBar = {
            HeaderCmp(
                onClickBack = onClickBack
            )
        },
        content = {
            Column(modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background)
            ) {
                Column(
                    modifier = Modifier
                        .padding(PADDING_16)
                ) {
                    val text1 = listOf(
                        AnnotatedString(
                            use_the_bank_reference_to_pay_at_participanting_banks_text,
                            textGrey700SpanStyle
                        ),
                    )

                    AutoResizeTextCmp(
                        modifier = Modifier
                            .fillMaxWidth(),
                        fontSize = FONT_SIZE_16,
                        textAlign = TextAlign.Center,
                        overflow = TextOverflow.Ellipsis,
                        annotatedStringList = text1
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_16)

                    LazyColumn(
                        modifier = Modifier) {
                        items(references.value.arrayReferences) { item ->

                            val url = if(item.images.size > 0) {
                                item.images[0].url3X3 ?: run {
                                    StringUtil.EMPTY_STR
                                }
                            }else{
                                StringUtil.EMPTY_STR
                            }

                            ItemCmp(
                                ref = item.reference?: StringUtil.EMPTY_STR,
                                image = url
                            )

                        }
                    }
                }
            }
        },
        bottomBar = {
        }
    )


}

@Composable
@Preview(showBackground = true)
fun WelcomeViewPreview(){
    WelcomeView(
        onClickBack = {},
        references = remember {
            mutableStateOf(ReferencesResponseModel())
        }
    )
}