package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil
import com.example.testtotalplay.util.StringUtil.Companion.close_text
import com.example.testtotalplay.util.StringUtil.Companion.error_text

@Composable
fun ErrorAlertCmp(
    boolSeeErrorAlert: MutableState<Boolean>,
    strMessage: MutableState<String>,
    closeOnClick:() -> Unit,
) {
    FadeInDialogCmp(
        boolSeeDialog = boolSeeErrorAlert,
        titleDialog = error_text,
        seeClose = false,
        closeOnClick = {
            boolSeeErrorAlert.value = false
        },
        content = {

            val annotatedStrMessage =
                listOf(
                    AnnotatedString(strMessage.value, SpanStyle(
                        fontFamily = MuseoSans300,
                        color = MaterialTheme.colors.primary
                    )
                    )
                )

            AutoResizeTextCmp(
                modifier = Modifier
                    .fillMaxWidth(),
                fontSize = FONT_SIZE_14,
                textAlign = TextAlign.Start,
                overflow = TextOverflow.Ellipsis,
                annotatedStringList = annotatedStrMessage
            )

            CustomVerticalDividerCmp(height = PADDING_32)

            val annotatedStrClose=
                listOf(
                    AnnotatedString(close_text, SpanStyle(
                        fontFamily = MuseoSans700,
                        color = MaterialTheme.colors.primary
                    ))
                )

            Row(modifier = Modifier
                .fillMaxWidth(),
                horizontalArrangement = Arrangement.End,
            ) {
                AutoResizeTextCmp(
                    maxLines = 1,
                    modifier = Modifier
                        .clickable {
                            boolSeeErrorAlert.value = false
                            strMessage.value = StringUtil.EMPTY_STR
                            closeOnClick()
                        },
                    fontSize = FONT_SIZE_16,
                    textAlign = TextAlign.End,
                    overflow = TextOverflow.Ellipsis,
                    annotatedStringList = annotatedStrClose
                )
            }
        }
    )
}

@Composable
@Preview(showBackground = true)
fun ErrorAlertCmpPreview(){
    ErrorAlertCmp(
        boolSeeErrorAlert = remember { mutableStateOf(false) },
        strMessage = remember { mutableStateOf(StringUtil.EMPTY_STR)},
        closeOnClick = {}
    )
}