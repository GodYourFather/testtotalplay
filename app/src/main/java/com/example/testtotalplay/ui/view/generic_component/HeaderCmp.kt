package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.testtotalplay.R
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil
import com.example.testtotalplay.util.StringUtil.Companion.bank_references_text

@Composable
fun HeaderCmp(
    onClickBack:() -> Unit
) {
    Row(modifier = Modifier
        .background(Color.Black)
        .fillMaxWidth(WEIGHT_100)
    ) {
        Row(modifier = Modifier
            .fillMaxWidth(WEIGHT_100)
            .padding(PADDING_16)
        ) {
            Row(modifier = Modifier
                .fillMaxWidth(WEIGHT_15),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    modifier = Modifier
                        .clickable { onClickBack() },
                    painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24),
                    contentDescription = StringUtil.EMPTY_STR
                )
            }
            Row(modifier = Modifier
                .fillMaxWidth(WEIGHT_70),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                val text1 = listOf(
                    AnnotatedString(
                        bank_references_text,
                        SpanStyle(
                            fontFamily = MuseoSans700,
                            color = Color.White
                        )
                    ),
                )

                AutoResizeTextCmp(
                    modifier = Modifier
                        .fillMaxWidth(),
                    fontSize = FONT_SIZE_16,
                    textAlign = TextAlign.Center,
                    overflow = TextOverflow.Ellipsis,
                    annotatedStringList = text1
                )
            }
            Row(modifier = Modifier
                .fillMaxWidth(),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_user_white),
                    contentDescription = StringUtil.EMPTY_STR
                )
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
fun HeaderCmpPreview(){
    HeaderCmp(
        onClickBack = {}
    )
}