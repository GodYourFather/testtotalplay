package com.example.testtotalplay.ui.view.welcome.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testtotalplay.BaseViewModel
import com.example.testtotalplay.data.ReferencesResponseModel
import com.example.testtotalplay.domain.GetListReferencesUseCase
import com.example.testtotalplay.util.StringUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WelcomeViewModel @Inject constructor(
    var getListReferencesUseCase: GetListReferencesUseCase,
) : ViewModel() {

    val baseViewModel = BaseViewModel()

    private var listReferences: MutableState<ReferencesResponseModel> = mutableStateOf(ReferencesResponseModel())

    fun getValueListReferences(): MutableState<ReferencesResponseModel>{
        return listReferences
    }

    fun setValueListReferences(value: ReferencesResponseModel){
        listReferences.value = value
    }

    fun getListReferences(){
        baseViewModel.showLoader()
        viewModelScope.launch {
            val result = getListReferencesUseCase()
            result.first?.let { references ->
                listReferences.value = references
                baseViewModel.hideLoader()
            }?: run{
                result.second?.let {
                    baseViewModel.setErrorAlertMessage(it.session?: StringUtil.EMPTY_STR)
                    baseViewModel.showErrorAlert()
                    baseViewModel.hideLoader()
                }
            }
        }
    }
    init {
        getListReferences()
    }
}