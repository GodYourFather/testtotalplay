package com.example.testtotalplay.ui.view.login.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testtotalplay.BaseViewModel
import com.example.testtotalplay.domain.LoginUseCase
import com.example.testtotalplay.util.ResourceState
import com.example.testtotalplay.util.StringUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    var loginUseCase: LoginUseCase,
) : ViewModel() {

    val baseViewModel = BaseViewModel()

    private val user: MutableState<String> = mutableStateOf(StringUtil.EMPTY_STR)

    fun getValueUser(): MutableState<String>{
        return user
    }

    fun setValueUser(value: String){
        user.value = value
    }

    private val password: MutableState<String> = mutableStateOf(StringUtil.EMPTY_STR)

    fun getValuePassword(): MutableState<String>{
        return password
    }

    fun setValuePassword(value: String){
        password.value = value
    }

    private val serviceLogin: MutableStateFlow<ResourceState> = MutableStateFlow(ResourceState.Default)

    fun getValueServiceLogin(): MutableStateFlow<ResourceState>{
        return serviceLogin
    }

    fun setValueServiceLogin(state: ResourceState){
        serviceLogin.value = state
    }

    fun login(){
        baseViewModel.showLoader()
        setValueServiceLogin(ResourceState.Loading)
        viewModelScope.launch {
            val result = loginUseCase(user = user.value, password = password.value)
            result.first?.let { session ->
                baseViewModel.hideLoader()
                setValueServiceLogin(ResourceState.Success)
            }?: run{
                result.second?.let {
                    baseViewModel.setErrorAlertMessage(it.session?: StringUtil.EMPTY_STR)
                    baseViewModel.showErrorAlert()
                    baseViewModel.hideLoader()
                    setValueServiceLogin(ResourceState.Failure)
                }
            }
        }
    }
}