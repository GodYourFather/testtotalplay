package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.example.testtotalplay.ui.theme.HEIGHT_8

@Composable
fun CustomVerticalDividerCmp(
    modifier: Modifier = Modifier,
    color: Color = Color.Transparent,
    height: Dp = HEIGHT_8
) {
    Divider(modifier, color, height)
}

@Composable
@Preview
fun CustomVerticalDividerCmpPreview(){
    CustomVerticalDividerCmp()
}