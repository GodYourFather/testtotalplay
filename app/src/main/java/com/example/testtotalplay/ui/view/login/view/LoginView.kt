package com.example.testtotalplay.ui.view.login.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.testtotalplay.R
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.ui.view.generic_component.AutoResizeTextCmp
import com.example.testtotalplay.ui.view.generic_component.ButtonCmp
import com.example.testtotalplay.ui.view.generic_component.CustomSimpleInputCmp
import com.example.testtotalplay.ui.view.generic_component.CustomVerticalDividerCmp
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil
import com.example.testtotalplay.util.StringUtil.Companion.account_email_and_phone_text
import com.example.testtotalplay.util.StringUtil.Companion.dont_have_a_password_text
import com.example.testtotalplay.util.StringUtil.Companion.i_forgot_my_password_text
import com.example.testtotalplay.util.StringUtil.Companion.log_in_text
import com.example.testtotalplay.util.StringUtil.Companion.log_in_to_your_account_text
import com.example.testtotalplay.util.StringUtil.Companion.password_text
import com.example.testtotalplay.util.StringUtil.Companion.sign_up_here_text

@Composable
fun LoginView(
    onClickContinue:() -> Unit,
    user: MutableState<String>,
    password: MutableState<String>
) {

    Scaffold(
        topBar = {
        },
        content = {
            Column(modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background)
            ) {
                Column(
                    modifier = Modifier
                        .padding(PADDING_16)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_totalplay),
                        contentDescription = StringUtil.EMPTY_STR
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_16)

                    Image(
                        modifier = Modifier
                            .width(IMAGE_SIZE_50)
                            .height(IMAGE_SIZE_50)
                        ,
                        painter = painterResource(id = R.drawable.ic_user),
                        contentDescription = StringUtil.EMPTY_STR
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_16)

                    val text1 = listOf(
                        AnnotatedString(
                            log_in_to_your_account_text,
                            textGrey700SpanStyle
                        ),
                    )

                    AutoResizeTextCmp(
                        modifier = Modifier
                            .fillMaxWidth(),
                        fontSize = FONT_SIZE_24,
                        textAlign = TextAlign.Start,
                        overflow = TextOverflow.Ellipsis,
                        annotatedStringList = text1
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_32)

                    CustomSimpleInputCmp(
                        labelText = account_email_and_phone_text,
                        textAlign = TextAlign.Start,
                        textFieldValue = TextFieldValue(
                            text = user.value,
                            selection = TextRange(
                                user.value.length
                            )
                        ),
                        keyboardType = KeyboardType.Text,
                        onValueChange = { textValue ->
                            user.value = textValue.text
                        }
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_16)

                    CustomSimpleInputCmp(
                        labelText = password_text,
                        textAlign = TextAlign.Start,
                        textFieldValue = TextFieldValue(
                            text = password.value,
                            selection = TextRange(
                                password.value.length
                            )
                        ),
                        keyboardType = KeyboardType.Password,
                        visualTransformation = PasswordVisualTransformation(),
                        trailingIcon = {
                            if (password.value.isNotEmpty()) {
                                IconButton(onClick = {
                                    password.value = StringUtil.EMPTY_STR
                                }) {
                                    Icon(
                                        imageVector = Icons.Filled.Clear,
                                        contentDescription = StringUtil.EMPTY_STR,
                                        tint = Color.White
                                    )
                                }
                            }
                        },
                        onValueChange = { textValue ->
                            password.value = textValue.text
                        }
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_16)

                    val text2 = listOf(
                        AnnotatedString(
                            i_forgot_my_password_text,
                            blue500SpanStyle
                        ),
                    )

                    AutoResizeTextCmp(
                        modifier = Modifier
                            .fillMaxWidth(),
                        fontSize = FONT_SIZE_16,
                        textAlign = TextAlign.Start,
                        overflow = TextOverflow.Ellipsis,
                        annotatedStringList = text2
                    )
                }
            }


        },
        bottomBar = {
            Column(modifier = Modifier
                .background(MaterialTheme.colors.background)
            ) {
                Column(
                    modifier = Modifier
                        .padding(PADDING_16)
                ) {
                    ButtonCmp(
                        text = log_in_text,
                        onClick = {onClickContinue()}
                    )

                    CustomVerticalDividerCmp(height = DIVIDER_16)

                    val text3 = listOf(
                        AnnotatedString(
                            dont_have_a_password_text,
                            SpanStyle(
                                fontFamily = MuseoSans300,
                                color = MaterialTheme.colors.primary
                            ),

                            ),
                        AnnotatedString(
                            sign_up_here_text,
                            blue500SpanStyle
                        )
                    )

                    AutoResizeTextCmp(
                        modifier = Modifier
                            .fillMaxWidth(),
                        fontSize = FONT_SIZE_16,
                        textAlign = TextAlign.Center,
                        overflow = TextOverflow.Ellipsis,
                        annotatedStringList = text3
                    )
                }
            }
        }
    )
}

@Composable
@Preview(showBackground = true)
fun LoginViewPreview(){
    LoginView(
        onClickContinue = {},
        user = remember {
            mutableStateOf(StringUtil.EMPTY_STR)
        },
        password = remember {
            mutableStateOf(StringUtil.EMPTY_STR)
        }
    )
}