package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.maximumHeightPercent
import com.example.testtotalplay.util.StringUtil

@Composable
fun FadeInDialogCmp(
    modifier: Modifier = Modifier,
    boolSeeDialog: MutableState<Boolean>,
    titleDialog: String,
    seeClose: Boolean,
    closeOnClick:() -> Unit,
    content: @Composable () -> Unit,
) {
    val scrollState = rememberScrollState()

    ConstraintLayout(modifier = modifier
        .fillMaxSize()
    ) {
        val (
            buttonsSocialMedia
        ) = createRefs()

        if (boolSeeDialog.value) {
            ConstraintLayout(
                modifier = modifier
                    .fillMaxSize()
                    .clickable(enabled = false, onClick = {})
                    .background(DefaultBackGroundDialogIntercam)
            ) {}
        }
        AnimatedVisibility(
            modifier = Modifier
                .constrainAs(buttonsSocialMedia) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(parent.bottom)
                    top.linkTo(parent.top)
                },
            visible = boolSeeDialog.value,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Surface(
                modifier = Modifier
                    .background(Color.Transparent)
                    .heightIn(min = HEIGHT_0, max = maximumHeightPercent(PERCENTAGE_90))
                    .fillMaxWidth(WEIGHT_90),
                color = MaterialTheme.colors.background,
                shape = RoundedCornerShape(ROUNDER_CORNER_SHAPE_4)
            ) {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colors.background)
                ) {
                    val annotatedStrText1 =
                        listOf(
                            AnnotatedString(
                                titleDialog,
                                SpanStyle(
                                    fontFamily = MuseoSans700,
                                    color = MaterialTheme.colors.primary
                                )
                            )
                        )
                    HeaderDialogCmp(
                        annotatedStrTitle = annotatedStrText1,
                        seeClose = seeClose,
                        seeDivider = false,
                        onClickClose = {
                            closeOnClick()
                        }
                    )
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .padding(PADDING_16)
                        .verticalScroll(scrollState)
                    ){
                        content()
                    }
                }
            }
        }
    }

}

@Composable
@Preview(showBackground = true)
fun FadeInDialogCmpPreview(){
    FadeInDialogCmp(
        boolSeeDialog = remember { mutableStateOf(true) },
        titleDialog = StringUtil.EMPTY_STR,
        seeClose = true,
        closeOnClick = {},
        content = {}
    )
}