package com.example.testtotalplay.ui.theme

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp



val FONT_SIZE_14 = 14.sp
val FONT_SIZE_16 = 16.sp
val FONT_SIZE_18 = 18.sp
val FONT_SIZE_24 = 24.sp

val PADDING_16 = 16.dp
val PADDING_32 = 32.dp

val DIVIDER_1 = 1.dp
val DIVIDER_8 = 8.dp
val DIVIDER_16 = 16.dp
val DIVIDER_32 = 32.dp

val IMAGE_SIZE_50 = 50.dp

val ELEVATION_4 = 4.dp

const val ROUNDER_CORNER_SHAPE_4 = 4

const val WEIGHT_15 = .15f
const val WEIGHT_70 = .7f
const val WEIGHT_90 = .9f
const val WEIGHT_100 = 1f

val HEIGHT_0 = 0.dp
val HEIGHT_8 = 8.dp
val HEIGHT_10 = 10.dp
val HEIGHT_60 = 60.dp

val WIDTH_0 = 0.dp

const val PERCENTAGE_90 = 90

val BUTTON_HEIGHT = 50.dp