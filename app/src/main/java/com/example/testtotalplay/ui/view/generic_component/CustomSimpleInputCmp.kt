package com.example.testtotalplay.ui.view.generic_component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.testtotalplay.ui.theme.*
import com.example.testtotalplay.util.AnnotatedString
import com.example.testtotalplay.util.StringUtil

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CustomSimpleInputCmp(
    modifier: Modifier = Modifier,
    labelText: String = StringUtil.EMPTY_STR,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Default,
    keyboardAction: () -> Unit = {},
    textFieldValue: TextFieldValue = TextFieldValue(),
    onValueChange: (TextFieldValue) -> Unit,
    trailingIcon: @Composable (() -> Unit)? = null,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    textAlign: TextAlign = TextAlign.Start,
    labelColor: Color = Text,
    fontColor: Color = Text,
    unfocusedIndicatorColor: Color = Text,
    focusedIndicatorColor: Color = Text,
    backgroundColor: Color = InputGrey,
    textFieldModifier: Modifier = Modifier,
) {

    val unfocusedLabelColor = if(textFieldValue.text.length > 0){
        labelColor
    }else{
        fontColor
    }

    val keyboardController = LocalSoftwareKeyboardController.current

    Column(
        modifier = modifier
            .fillMaxWidth()
            .height(HEIGHT_60)
    ) {

        TextField(
            modifier = textFieldModifier
                .fillMaxWidth(),
            value = textFieldValue,
            singleLine = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = backgroundColor,
                unfocusedIndicatorColor = unfocusedIndicatorColor,
                focusedIndicatorColor = focusedIndicatorColor,
                textColor = fontColor,
                unfocusedLabelColor = unfocusedLabelColor,
                focusedLabelColor = labelColor
            ),
            visualTransformation = visualTransformation,
            textStyle = LocalTextStyle.current.copy(
                fontSize = FONT_SIZE_18,
                textAlign = textAlign
            ),
            keyboardOptions = KeyboardOptions(
                keyboardType = keyboardType,
                imeAction = imeAction,
                autoCorrect = false
            ),
            keyboardActions = KeyboardActions {
                keyboardController?.hide()
                keyboardAction()
            },
            onValueChange = onValueChange,
            label = {

                val annotatedStr = listOf(
                    AnnotatedString(
                        labelText,
                        SpanStyle(
                            fontFamily = MuseoSans500,
                        )
                    ),
                )

                AutoResizeTextCmp(
                    modifier = Modifier
                        .fillMaxWidth(),
                    fontSize = FONT_SIZE_16,
                    textAlign = textAlign,
                    overflow = TextOverflow.Ellipsis,
                    annotatedStringList = annotatedStr
                )
            },
            trailingIcon = trailingIcon
        )
    }
}

@Composable
@Preview(showBackground = true)
fun CustomSimpleInputCmpPreview() {
    CustomSimpleInputCmp(
        modifier = Modifier,
        labelText = StringUtil.test_text,
        textAlign = TextAlign.Center,
        keyboardType = KeyboardType.Text,
        textFieldValue = TextFieldValue(StringUtil.test_text),
        onValueChange = {},
        visualTransformation = VisualTransformation.None)
}