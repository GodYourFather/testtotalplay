package com.example.testtotalplay.core

import com.example.testtotalplay.data.LoginRequestModel
import com.example.testtotalplay.data.ReferencesResponseModel
import com.example.testtotalplay.data.SessionModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiClient {
    @POST("{url}")
    suspend fun postLogin(@Path(value = "url", encoded = true) url: String, @Body entity: LoginRequestModel): Response<SessionModel>

    @POST("{url}")
    suspend fun postReferences(@Path(value = "url", encoded = true) url: String, @Body entity: SessionModel): Response<ReferencesResponseModel>

}