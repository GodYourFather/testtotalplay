package com.example.testtotalplay.data

import com.google.gson.annotations.SerializedName

data class ReferencesResponseModel (
    @SerializedName("status"          ) var status          : Int?                       = null,
    @SerializedName("arrayReferences" ) var arrayReferences : ArrayList<ReferenceModel> = arrayListOf()
)