package com.example.testtotalplay.data

import com.example.testtotalplay.util.StringUtil
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CipherModel(
    @SerializedName("value") var value: String = StringUtil.EMPTY_STR
): Serializable