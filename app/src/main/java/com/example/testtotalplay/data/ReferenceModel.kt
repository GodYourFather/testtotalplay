package com.example.testtotalplay.data

import com.google.gson.annotations.SerializedName

data class ReferenceModel (

    @SerializedName("images"    ) var images    : ArrayList<ImageModel> = arrayListOf(),
    @SerializedName("bank"      ) var bank      : String?           = null,
    @SerializedName("reference" ) var reference : String?           = null,
    @SerializedName("aliasbank" ) var aliasbank : String?           = null

)
