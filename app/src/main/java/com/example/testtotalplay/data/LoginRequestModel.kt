package com.example.testtotalplay.data

import com.google.gson.annotations.SerializedName

data class LoginRequestModel(
    @SerializedName("user") val user: String,
    @SerializedName("password") val password: String
)