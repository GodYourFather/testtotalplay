package com.example.testtotalplay.data

import com.google.gson.annotations.SerializedName

data class SessionModel(
    @SerializedName("session") var session: String = "",
)