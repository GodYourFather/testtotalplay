package com.example.testtotalplay.data

import com.google.gson.annotations.SerializedName

data class ImageModel (
    @SerializedName("url3X3" ) var url3X3 : String? = null,
    @SerializedName("url4X4" ) var url4X4 : String? = null,
    @SerializedName("url5X5" ) var url5X5 : String? = null,
    @SerializedName("url6X6" ) var url6X6 : String? = null,
    @SerializedName("url7X7" ) var url7X7 : String? = null
)
