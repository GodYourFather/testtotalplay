package com.example.testtotalplay.util

import androidx.compose.ui.text.SpanStyle

data class AnnotatedString(
    var text: String,
    val spanStyle: SpanStyle
)
