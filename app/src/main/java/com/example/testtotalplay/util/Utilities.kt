package com.example.testtotalplay.util

import android.util.Log
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.reflect.Type

class Utilities {
    companion object {
        fun genericParser(jsonString: String, type: Type): Any? {
            try {
                val jsonObject = JSONObject(jsonString)

                return Gson().fromJson(jsonObject.toString(), type)

            } catch (e: Exception) {

                Log.d("GENERIC_PARSER", "Ocurrió un error al parsear $e")

            }

            return null
        }
    }
}