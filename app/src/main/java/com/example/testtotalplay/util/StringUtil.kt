package com.example.testtotalplay.util

class StringUtil{
    companion object{
        val EMPTY_STR = ""

        val test_text = "Texto de prueba"
        val error_text = "Error"
        val close_text = "Cerrar"
        val bank_references_text = "Referencias bancarias"
        val log_in_to_your_account_text = "Ingresa a tu cuenta."
        val account_email_and_phone_text = "Cuenta, correo y teléfono"
        val i_forgot_my_password_text = "Olvide mi contraseña"
        val password_text = "Contraseña"
        val log_in_text = "Iniciar sesión"
        val dont_have_a_password_text = "¿No tienes contraseña? "
        val sign_up_here_text = "Registrate aquí"

        val use_the_bank_reference_to_pay_at_participanting_banks_text = "Utiliza la referencia bancaria para pagar en los bancos participantes."

    }
}