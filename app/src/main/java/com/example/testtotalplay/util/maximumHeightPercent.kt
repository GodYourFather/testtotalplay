package com.example.testtotalplay.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun maximumHeightPercent(percent:Int): Dp {

    val configuration = LocalConfiguration.current

    val screenHeight = configuration.screenHeightDp * (".$percent").toDouble()

    return screenHeight.dp

}