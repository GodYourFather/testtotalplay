package com.example.testtotalplay.util

import com.example.testtotalplay.data.SessionModel


class SessionManager {
    private var sessionRequest: SessionModel = SessionModel()
    companion object{
        private var sharedInstance: SessionManager? = null

        fun getInstance(): SessionManager {
            if (sharedInstance == null) {
                sharedInstance = SessionManager()
            }
            return sharedInstance!!
        }
    }

    fun getSession() : SessionModel {
        return sessionRequest
    }

    fun setSession(baseRequest: SessionModel) {
        this.sessionRequest = baseRequest
    }

    fun setValueSession(token: String?) {
        token?.let {
            if (it != StringUtil.EMPTY_STR) {
                this.sessionRequest.session = it
            }
        }
    }

    fun clean(){
        sessionRequest = SessionModel()
    }
}