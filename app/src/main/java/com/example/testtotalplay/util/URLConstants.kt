package com.example.testtotalplay.util

class URLConstants {
    companion object {
        const val urlBase = "https://ott.totalplay.com.mx/AppTest0/"
        const val urlLogin = "cliente.do"
        const val urlReferences = "clienteresp.do"
    }
}