package com.example.testtotalplay.util

sealed class ResourceState {
    object Default : ResourceState()
    object Loading : ResourceState()
    object Success: ResourceState()
    object Failure: ResourceState()
}
