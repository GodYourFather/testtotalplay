package com.example.testtotalplay.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.testtotalplay.ui.view.login.screen.LoginScreen
import com.example.testtotalplay.ui.view.welcome.screen.WelcomeScreen

@Composable
fun SetupNavGraph(navController: NavHostController) {

    NavHost(
        navController = navController,
        startDestination = Destinations.Login.route
    ) {
        //Home--------------------------------------------------------------------------------------
        loginScreen(navController = navController)
        //------------------------------------------------------------------------------------------
        //Login-------------------------------------------------------------------------------------
        welcomeScreen(navController = navController)
        //------------------------------------------------------------------------------------------
    }
}

fun NavGraphBuilder.loginScreen(
    navController: NavHostController
) {
    composable(
        route = Destinations.Login.route,

    ) {
        LoginScreen(navController = navController)
    }
}

fun NavGraphBuilder.welcomeScreen(
    navController: NavHostController
) {
    composable(
        route = Destinations.Welcome.route,
    ) {
        WelcomeScreen(navController = navController)
    }
}