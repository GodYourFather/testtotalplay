package com.example.testtotalplay.navigation

sealed class Destinations(
    val route: String
) {
    object Login : Destinations("login_screen")
    object Welcome : Destinations("welcome_screen")
}