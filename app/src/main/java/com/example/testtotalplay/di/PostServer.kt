package com.example.testtotalplay.di

import com.example.testtotalplay.core.ApiClient
import com.example.testtotalplay.data.LoginRequestModel
import com.example.testtotalplay.data.ReferencesResponseModel
import com.example.testtotalplay.data.SessionModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class PostServer @Inject constructor(private val api: ApiClient) {

    suspend fun getLoginPostServer(endpoint: String, cipher: LoginRequestModel): Response<SessionModel> {

        return withContext(Dispatchers.IO) {
            val response = api.postLogin(endpoint, cipher)
            response
        }
    }

    suspend fun getReferencesPostServer(endpoint: String, cipher: SessionModel): Response<ReferencesResponseModel> {

        return withContext(Dispatchers.IO) {
            val response = api.postReferences(endpoint, cipher)
            response
        }
    }

}