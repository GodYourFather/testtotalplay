package com.example.testtotalplay.di

import com.example.testtotalplay.data.LoginRequestModel
import com.example.testtotalplay.data.ReferencesResponseModel
import com.example.testtotalplay.data.SessionModel
import retrofit2.Response
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val api: PostServer,
) {
    suspend fun getPostRepository(endpoint: String, cipher: LoginRequestModel): Response<SessionModel> {
        return api.getLoginPostServer(endpoint, cipher)
    }

    suspend fun getReferencesPostRepository(endpoint: String, cipher: SessionModel): Response<ReferencesResponseModel> {
        return api.getReferencesPostServer(endpoint, cipher)
    }
}