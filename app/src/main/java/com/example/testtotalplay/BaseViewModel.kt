package com.example.testtotalplay

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.testtotalplay.util.StringUtil

class BaseViewModel {

    private val seeLoader: MutableState<Boolean> = mutableStateOf(false)

    fun getBoolSeeLoader(): MutableState<Boolean>{
        return seeLoader
    }
    fun showLoader(){
        seeLoader.value = true
    }
    fun hideLoader(){
        seeLoader.value = false
    }

    //----------------------------------------------------------------------------------------------

    private val seeErrorAlert: MutableState<Boolean> = mutableStateOf(false)
    fun getBoolSeeErrorAlert(): MutableState<Boolean>{
        return seeErrorAlert
    }
    fun showErrorAlert(){
        seeErrorAlert.value = true
    }
    fun hideErrorAlert(){
        seeErrorAlert.value = false
    }

    //----------------------------------------------------------------------------------------------

    private val strErrorAlertMessage: MutableState<String> = mutableStateOf(StringUtil.EMPTY_STR)
    fun setErrorAlertMessage(str: String){
        strErrorAlertMessage.value = str
    }
    fun getErrorAlertMessage(): MutableState<String>{
        return strErrorAlertMessage
    }

    //----------------------------------------------------------------------------------------------

}